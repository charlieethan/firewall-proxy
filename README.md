# 关于
本项目旨在提供聚合式的代理软件搭建教程，包括常用的 **Shadowsocks** **V2ray** 以及正在开发的 **Trojan**  
适用的平台为 **Windows 7，Windows 10** 以及 **安卓 6 ~ 安卓 10**
# 内容
- Shadowsocks+obfs ：https://github.com/charlieethan/firewall-proxy/tree/master/shadowsocks  
- V2ray+mkcp（无需域名）：https://github.com/charlieethan/firewall-proxy/tree/master/V2ray/V2ray%2Bmkcp  
- V2ray+websocket+tls+cdn (需要域名）：https://github.com/charlieethan/firewall-proxy/tree/master/V2ray/V2ray%2BCDN
- Trojan（需要域名）：https://github.com/charlieethan/firewall-proxy/tree/master/Trojan
# 注意事项
- **需要域名** 的搭建方式需要你 **拥有一个自己的域名**并会**正确设置解析记录**     
由于互联网上关于此的教程数以万计，因此**如果你不会请自行学习**，教程中不再赘述基础知识
# 有潜力的项目      
目前三大代理软件，各有各的客户端，切换或更新时要分别处理，相当麻烦。  
目前有两个项目，致力于将三大协议写入一个软件：      
- PC端（Linux & Windows & MacOS) ：https://github.com/Dr-Incognito/V2Ray-Desktop     
- Android端 ：https://github.com/PharosVip/Pharos-Android-Test    
- PC端的缺点 ：1）Shadowsocks只支持使用混淆的协议 2）V2ray不支持mkcp协议    
- Android端的缺点 ：1）仍在Beta阶段，不支持手动配置，只支持链接导入  
